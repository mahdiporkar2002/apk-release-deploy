#!/usr/bin/python3
#
# Copyright (C) 2019 Oleg Shnaydman
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os
import argparse
import requests
import json
import re
import shutil

TEMPLATE_ERROR_CODE = 3
CHANGES_ERROR_CODE = 4
OUTPUT_FILE_PARSING_ERROR = 5
TELEGRAM_ERROR_CODE = 6

def send_message_to_telegram(bot_token, chat_id, caption=''):
    '''Send a message to a Telegram chat.

    Args:
        bot_token (str): The Telegram Bot API token used to authenticate with Telegram.
        chat_id (str): The unique identifier of the chat or recipient.
        caption (str, optional): The text message to be sent as a caption (default is an empty string).

    Returns:
        bool: True if the message was sent successfully, False otherwise.
    '''
    try:
        # Construct the URL for sending a message to Telegram using the provided bot_token
        url = f'https://api.telegram.org/bot{bot_token}/sendMessage'

        # Prepare the data to be sent in the POST request
        data = {
            'chat_id': chat_id,
            'text': caption,
        }

        # Send the POST request to the Telegram API
        response = requests.post(url, data=data)
        
        # Check if the request was successful (HTTP status code 200)
        if response.status_code == 200:
            print('Message sent to telegram successfully')
            return True
        else:
            # If the request was not successful, print an error message and the response text
            print(
                f'Failed to send message. Status code: {response.status_code}')
            print(response.text)

            return False
    except Exception as e:
        # Handle any exceptions that may occur during the execution of the code
        print(f'Error: {e}')
        return False


def send_to_telegram(bot_token, chat_id, app_file, target_app_file, caption=''):
    '''Send a document (file) to a Telegram chat and optionally include a caption.

    Args:
        bot_token (str): The Telegram Bot API token used to authenticate with Telegram.
        chat_id (str): The unique identifier of the chat or recipient.
        app_file (str): The path to the source document (file) to be sent.
        target_app_file (str): The path to the copied document (file) to be sent.
        caption (str, optional): The caption text for the document (default is an empty string).

    Returns:
        bool: True if the document was uploaded successfully, False otherwise.
    '''
    try:
        # Copy the source document to the target location
        shutil.copy(app_file, target_app_file)

        # Construct the URL for sending a document to Telegram using the provided bot_token
        url = f'https://api.telegram.org/bot{bot_token}/sendDocument'

        # Prepare the data to be sent in the POST request
        data = {
            'chat_id': chat_id,
            'caption': caption,
        }

        # Define the files to be sent (document with its file name)
        files = {'document': (target_app_file, open(target_app_file, 'rb'))}

        # Send the POST request to the Telegram API to upload the document
        response = requests.post(url, data=data, files=files)

        # Check if the request was successful (HTTP status code 200)
        if response.status_code == 200:
            print('Document uploaded to telegram successfully')
            return True
        else:
            # If the upload request was not successful, print an error message and the response text
            print(
                f'Failed to upload document. Status code: {response.status_code}')
            print(response.text)

            # Send a message to Telegram with the provided caption to notify about the failure
            send_message_to_telegram(
                bot_token=bot_token, chat_id=chat_id, caption=caption)
            return False
    except Exception as e:
        # Handle any exceptions that may occur during the execution of the code
        print(f'Error: {e}')

        # Send a message to Telegram with the provided caption to notify about the error
        send_message_to_telegram(
            bot_token=bot_token, chat_id=chat_id, caption=caption)
        return False


def get_app(release_dir):
    '''Extract app data
    
    Args:
        release_dir (str): Path to release directory.

    Returns:
        (str, str): App version and path to release apk file.
    '''
    output_path = os.path.join(release_dir, 'output-metadata.json')

    with(open(output_path)) as app_output:
        json_data = json.load(app_output)
        app_version = json_data['elements'][0]['versionName']
        app_file = os.path.join(release_dir, json_data['elements'][0]['outputFile'])
        return app_version, app_file

    return None, None

def get_target_file_name(app_name, app_version):
    '''Generate file name for released apk, using app name and version:
    app_name - MyApp
    version - 1.03
    result: myapp_1_03.apk
    
    Args:
        app_name (str): App name.
        app_version (str): App version.

    Returns:
        str: App file name.
    '''
    app_name = app_name.lower()
    app_version = app_version.replace('.', '_')
    return '{name}_{version}.apk'.format(name=app_name, version=app_version).replace(' ','')


def get_changes(change_log_path):
    '''Extract latest changes from changelog file.
    Changes are separated by ##

    Args:
        change_log_path (str): Path to changelog file.

    Returns:
        str: Latest changes.
    '''
    with(open(change_log_path)) as change_log_file:
        change_log = change_log_file.read()

    # Split by '##' and remove lines starting with '#'
    latest_version_changes = change_log.split('##')[0][:-1]
    latest_version_changes = re.sub('^#.*\n?', '', latest_version_changes, flags=re.MULTILINE)

    return latest_version_changes


def get_email(app_name, app_version, changes, template_file_path):
    '''Use template file to create release email subject and title.

    Args:
        app_name (str): App name.
        app_version (str): App version.
        changes (str): Lastest app changelog.
        template_file_path (str): Path to template file.

    Returns:
        (str, str): Email subject and email body.
    '''
    target_subject = 1
    target_body = 2
    target = 0

    subject = ''
    body = ''

    template = ''

    with(open(template_file_path)) as template_file:
        # Open template file and replace placeholders with data
        template = template_file.read().format(
            change_log=changes,
            app_name=app_name,
            app_version=app_version
        )
        
    # Iterate over each line and collect lines marked for subject/body
    for line in template.splitlines():
        if line.startswith('#'):
            if line.startswith('#subject'):
                target = target_subject
            elif line.startswith('#body'):
                target = target_body
        else:
            if target == target_subject:
                subject += line + '\n'
            elif target == target_body:
                body += line + '\n'
    
    return subject.rstrip(), body.rstrip()


if __name__ == '__main__':
    # Command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--release.dir', dest='release_dir', help='path to release folder', required=True)
    parser.add_argument('--app.name', dest='app_name', help='app name that will be used as file name', required=True)
    parser.add_argument('--changelog.file', dest='changelog_file', help='path to changelog file', required=True)
    parser.add_argument('--template.file', dest='template_file', help='path to email template file', required=True)
    parser.add_argument('--telegram.token', dest='telegram_token', help='telegram bot token', required=True)
    parser.add_argument('--telegram.chat_id', dest='telegram_chat_id', help='telegram chat id', required=True)

    options = parser.parse_args()

    # Extract app version and file
    app_version, app_file = get_app(options.release_dir)
    if app_version == None or app_file == None:
        exit(OUTPUT_FILE_PARSING_ERROR)
    
    target_app_file = get_target_file_name(options.app_name, app_version)

    # Extract latest changes
    latest_changes = get_changes(options.changelog_file)
    if latest_changes == None:
        exit(CHANGES_ERROR_CODE)
    
    # Compose email subject and body
    subject, body = get_email(options.app_name, app_version, latest_changes, options.template_file)
    if subject == None or body == None:
        exit(TEMPLATE_ERROR_CODE)

    # Send to telegram with release file & data
    if not send_to_telegram(options.telegram_token, options.telegram_chat_id, app_file, target_app_file, body):
        exit(TELEGRAM_ERROR_CODE)
